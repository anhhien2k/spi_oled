#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdint.h>
#include <stdio.h>
#include "driver/spi_master.h"
#include "esp_log.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"
#include <stddef.h>
#include <string.h>
#include "driver/dac.h"
#include "driver/adc.h"

#include <stddef.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

/**
 * GPIO config
**/
#define DEV_DC_PIN  7
#define DEV_RST_PIN 9
#define DEV_BL_PIN  17
#define DEV_CS_PIN 14

/**
 * GPIO read and write
**/
#define DEV_Digital_Write(_pin, _value) gpio_set_level(_pin, _value == 0? 0 : 1)
#define DEV_Digital_Read(_pin) gpio_get_level(_pin)

/**
 * SPI
**/
#define DEV_SPI_WRITE(_handle, _dat)  spi_device_transmit(_handle, _dat)

/**
 * delay x ms
**/
#define DEV_Delay_ms(__xms)    vTaskDelay(__xms / portTICK_RATE_MS);
/**
 * PWM_BL
**/
#define  DEV_Set_BL(_Channel, _Value)  dac_output_voltage(_Channel, _Value);

void Config_Init();

#endif
