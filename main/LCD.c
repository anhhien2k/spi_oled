#include "LCD.h"

spi_transaction_t t;
spi_device_handle_t handle;

static void LCD_Reset(void)
{
	DEV_Digital_Write(DEV_CS_PIN, 0);
	DEV_Delay_ms(20);
	DEV_Digital_Write(DEV_RST_PIN, 0);
	DEV_Delay_ms(20);
	DEV_Digital_Write(DEV_RST_PIN, 1);
	DEV_Delay_ms(20);
}

/*******************************************************************************
function:
	Setting backlight
parameter	:
		value : Range 0~1000   Duty cycle is value/1000
*******************************************************************************/
void LCD_SetBacklight(uint16_t Value)
{
	DEV_Set_BL(DAC_CHANNEL_1, Value);
}

/*******************************************************************************
function:
		Write register address and data
*******************************************************************************/
void LCD_WriteData_Byte(uint8_t da)
{
	memset(&t, 0, sizeof(t));
	t.length=sizeof(uint8_t)*8; // 128*8
	t.tx_buffer=&da;
	DEV_Digital_Write(DEV_CS_PIN,0);
	DEV_Digital_Write(DEV_DC_PIN,1);
	DEV_SPI_WRITE(handle, &t);
	DEV_Digital_Write(DEV_CS_PIN,1);
}

 void LCD_WriteData_Word(uint16_t da)
{
	uint8_t i=(da>>8)&0xff;
	memset(&t, 0, sizeof(t));
	t.length=sizeof(uint8_t)*8; // 128*8
	t.tx_buffer=&i;
	DEV_Digital_Write(DEV_CS_PIN, 0);
	DEV_Digital_Write(DEV_DC_PIN, 1);
	DEV_SPI_WRITE(handle, &t);

	memset(&t, 0, sizeof(t));
	t.length=sizeof(uint8_t)*8; // 128*8
	t.tx_buffer=&da;
	DEV_SPI_WRITE(handle, &t);
	DEV_Digital_Write(DEV_CS_PIN, 1);
}

void LCD_WriteReg(uint8_t da)
{
	memset(&t, 0, sizeof(t));
	t.length=sizeof(uint8_t)*8; // 128*8
	t.tx_buffer=&da;
	DEV_Digital_Write(DEV_CS_PIN, 0);
	DEV_Digital_Write(DEV_DC_PIN, 0);
	DEV_SPI_WRITE(handle, &t);
	DEV_Digital_Write(DEV_CS_PIN, 1);
}

/******************************************************************************
function:
		Common register initialization
******************************************************************************/
void LCD_Init(void)
{
	LCD_Reset();

	//************* Start Initial Sequence **********//
    LCD_WriteReg(0x11);//Sleep exit
    DEV_Delay_ms (120);
    LCD_WriteReg(0x21);
    LCD_WriteReg(0x21);

    LCD_WriteReg(0xB1);
    LCD_WriteData_Byte(0x05);
    LCD_WriteData_Byte(0x3A);
    LCD_WriteData_Byte(0x3A);

    LCD_WriteReg(0xB2);
    LCD_WriteData_Byte(0x05);
    LCD_WriteData_Byte(0x3A);
    LCD_WriteData_Byte(0x3A);

    LCD_WriteReg(0xB3);
    LCD_WriteData_Byte(0x05);
    LCD_WriteData_Byte(0x3A);
    LCD_WriteData_Byte(0x3A);
    LCD_WriteData_Byte(0x05);
    LCD_WriteData_Byte(0x3A);
    LCD_WriteData_Byte(0x3A);

    LCD_WriteReg(0xB4);
    LCD_WriteData_Byte(0x03);

    LCD_WriteReg(0xC0);
    LCD_WriteData_Byte(0x62);
    LCD_WriteData_Byte(0x02);
    LCD_WriteData_Byte(0x04);

    LCD_WriteReg(0xC1);
    LCD_WriteData_Byte(0xC0);

    LCD_WriteReg(0xC2);
    LCD_WriteData_Byte(0x0D);
    LCD_WriteData_Byte(0x00);

    LCD_WriteReg(0xC3);
    LCD_WriteData_Byte(0x8D);
    LCD_WriteData_Byte(0x6A);

    LCD_WriteReg(0xC4);
    LCD_WriteData_Byte(0x8D);
    LCD_WriteData_Byte(0xEE);

    LCD_WriteReg(0xC5);  /*VCOM*/
    LCD_WriteData_Byte(0x0E);

    LCD_WriteReg(0xE0);
    LCD_WriteData_Byte(0x10);
    LCD_WriteData_Byte(0x0E);
    LCD_WriteData_Byte(0x02);
    LCD_WriteData_Byte(0x03);
    LCD_WriteData_Byte(0x0E);
    LCD_WriteData_Byte(0x07);
    LCD_WriteData_Byte(0x02);
    LCD_WriteData_Byte(0x07);
    LCD_WriteData_Byte(0x0A);
    LCD_WriteData_Byte(0x12);
    LCD_WriteData_Byte(0x27);
    LCD_WriteData_Byte(0x37);
    LCD_WriteData_Byte(0x00);
    LCD_WriteData_Byte(0x0D);
    LCD_WriteData_Byte(0x0E);
    LCD_WriteData_Byte(0x10);

    LCD_WriteReg(0xE1);
    LCD_WriteData_Byte(0x10);
    LCD_WriteData_Byte(0x0E);
    LCD_WriteData_Byte(0x03);
    LCD_WriteData_Byte(0x03);
    LCD_WriteData_Byte(0x0F);
    LCD_WriteData_Byte(0x06);
    LCD_WriteData_Byte(0x02);
    LCD_WriteData_Byte(0x08);
    LCD_WriteData_Byte(0x0A);
    LCD_WriteData_Byte(0x13);
    LCD_WriteData_Byte(0x26);
    LCD_WriteData_Byte(0x36);
    LCD_WriteData_Byte(0x00);
    LCD_WriteData_Byte(0x0D);
    LCD_WriteData_Byte(0x0E);
    LCD_WriteData_Byte(0x10);

    LCD_WriteReg(0x3A);
    LCD_WriteData_Byte(0x05);

    LCD_WriteReg(0x36);
    LCD_WriteData_Byte(0xA8);

    LCD_WriteReg(0x29);
}

/******************************************************************************
function:	Set the cursor position
parameter	:
		Xstart: 	Start uint16_t x coordinate
	  Ystart:		Start uint16_t y coordinate
	  Xend  :		End uint16_t coordinates
	  Yend  :		End uint16_t coordinatesen
******************************************************************************/
void LCD_SetCursor(uint16_t Xstart, uint16_t Ystart, uint16_t Xend, uint16_t  Yend)
{
	Xstart = Xstart + 1;
	Xend = Xend + 1;
	Ystart = Ystart + 26;
	Yend = Yend+26;
	LCD_WriteReg(0x2a);
	LCD_WriteData_Byte(Xstart>>8);
	LCD_WriteData_Byte(Xstart);
	LCD_WriteData_Byte(Xend >>8);
	LCD_WriteData_Byte(Xend );

	LCD_WriteReg(0x2b);
	LCD_WriteData_Byte(Ystart>>8);
	LCD_WriteData_Byte(Ystart);
	LCD_WriteData_Byte(Yend>>8);
	LCD_WriteData_Byte(Yend);

	LCD_WriteReg(0x2C);
}

/******************************************************************************
function:	Clear screen function, refresh the screen to a certain color
parameter	:
		Color :		The color you want to clear all the screen
******************************************************************************/
void LCD_Clear(uint16_t Color)
{
	uint16_t i,j;
	LCD_SetCursor(0,0,LCD_WIDTH-1,LCD_HEIGHT-1);
	for(i = 0; i < LCD_WIDTH; i++){
		for(j = 0; j < LCD_HEIGHT; j++){
			LCD_WriteData_Word(Color);
		}
	 }
}

/******************************************************************************
function:	Refresh a certain area to the same color
parameter	:
		Xstart: 	Start uint16_t x coordinate
	  Ystart:		Start uint16_t y coordinate
	  Xend  :		End uint16_t coordinates
	  Yend  :		End uint16_t coordinates
	  color :		Set the color
******************************************************************************/
void LCD_ClearWindow(uint16_t Xstart, uint16_t Ystart, uint16_t Xend, uint16_t Yend,uint16_t color)
{
	uint16_t i,j;
	LCD_SetCursor(Xstart, Ystart, Xend-1,Yend-1);
	for(i = Ystart; i <= Yend-1; i++){
		for(j = Xstart; j <= Xend-1; j++){
			LCD_WriteData_Word(color);
		}
	}
}

/******************************************************************************
function: Set the color of an area
parameter	:
		Xstart: 	Start uint16_t x coordinate
	  Ystart:		Start uint16_t y coordinate
	  Xend  :		End uint16_t coordinates
	  Yend  :		End uint16_t coordinates
	  Color :		Set the color
******************************************************************************/
void LCD_SetWindowColor(uint16_t Xstart, uint16_t Ystart, uint16_t Xend, uint16_t Yend,uint16_t  Color)
{
	LCD_SetCursor( Xstart,Ystart,Xend,Yend);
	LCD_WriteData_Word(Color);
}

/******************************************************************************
function: Draw a uint16_t
parameter	:
		X			: 	Set the X coordinate
	  Y			:		Set the Y coordinate
	  Color :		Set the color
******************************************************************************/
void LCD_Setuint16_t(uint16_t x, uint16_t y, uint16_t Color)
{
	LCD_SetCursor(x,y,x,y);
	LCD_WriteData_Word(Color);
}



