#ifndef __LCD_H
#define __LCD_H

#include "Config.h"

#define LCD_WIDTH   160 //LCD width
#define LCD_HEIGHT  80 //LCD height

void LCD_WriteData_Byte(uint8_t da);
void LCD_WriteData_Word(uint16_t da);
void LCD_WriteReg(uint8_t da);

void LCD_SetCursor(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void LCD_Setuint16_t(uint16_t x, uint16_t y, uint16_t Color);

void LCD_Init(void);
void LCD_SetBacklight(uint16_t Value);
void LCD_Clear(uint16_t Color);
void LCD_ClearWindow(uint16_t Xstart, uint16_t Ystart, uint16_t Xend, uint16_t Yend, uint16_t uint16_t);

#endif
