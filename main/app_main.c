#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "lwip/igmp.h"

#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "soc/rtc_periph.h"
#include "driver/spi_master.h"
#include "esp_log.h"
#include "esp_spi_flash.h"

#include "driver/gpio.h"
#include "esp_intr_alloc.h"

#include "LCD.h"
#include "fonts.h"
#include "GUI.h"
//#include "image.h"

#define GPIO_MOSI 12
#define GPIO_MISO 13
#define GPIO_SCLK 15
#define DEV_CS_PIN 14

// OLED
//#define DEV_CS_PIN  8
//#define DEV_DC_PIN  7

#define SENDER_HOST SPI2_HOST

spi_transaction_t t;
spi_device_handle_t handle;

// Init OLED pin
void GPIO_Init(){
    gpio_pad_select_gpio(DEV_CS_PIN);
    gpio_pad_select_gpio(DEV_RST_PIN);
    gpio_pad_select_gpio(DEV_DC_PIN);

    gpio_set_direction(DEV_CS_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(DEV_RST_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(DEV_DC_PIN, GPIO_MODE_OUTPUT);
    // DAC : DEV_BL_PIN : Pin 17
    dac_output_enable(DAC_CHANNEL_1);
}
char data[129]="";
void app_main(void)
{
    //Configuration for the SPI bus
    spi_bus_config_t buscfg={
        .mosi_io_num=GPIO_MOSI,
        .miso_io_num=GPIO_MISO,
        .sclk_io_num=GPIO_SCLK,
        .quadwp_io_num=-1,
        .quadhd_io_num=-1
    };

    //Configuration for the SPI device on the other side of the bus
    spi_device_interface_config_t devcfg={
        .command_bits=0,
        .address_bits=0,
        .dummy_bits=0,
        .clock_speed_hz= 40000000,
        .duty_cycle_pos=128,        //50% duty cycle
        .mode=0,
        .spics_io_num= DEV_CS_PIN,
        .cs_ena_posttrans=3,        //Keep the CS low 3 cycles after transaction, to stop slave from missing the last bit when CS has less propagation delay than CLK
        .queue_size=3
    };

//    char sendbuf[129] = {0};
//    //spi_transaction_t t;
//    spi_transaction_t t;
//    memset(&t, 0, sizeof(t));

    //Initialize the SPI bus and add the device we want to send stuff to.
    spi_bus_initialize(SENDER_HOST, &buscfg, SPI_DMA_CH_AUTO);

    spi_bus_add_device(SENDER_HOST, &devcfg, &handle);

    //Assume the slave is ready for the first transmission: if the slave started up before us, we will not detect
    //positive edge on the handshake line.

    GPIO_Init();
    LCD_SetBacklight(100);
	LCD_Init();
    LCD_Clear(BLACK);
    Paint_NewImage(LCD_WIDTH, LCD_HEIGHT,0,WHITE);
    float b = 10.5;

    while(1) {

    	Paint_DrawString_EN(10, 10, "ABC", &Font24, BLACK, CYAN);
    	vTaskDelay(2000 / portTICK_RATE_MS);

    	sprintf(data, "%.2f", b);
    	Paint_DrawString_EN(10, 10, data, &Font24, BLACK, CYAN);
    	vTaskDelay(2000 / portTICK_RATE_MS);
    	LCD_Clear(BLACK);
    	//memset(&data, 0, sizeof(data));

        //sprintf(sendbuf, "Data is sended: %04d.", n);

        //t.length=sizeof(sendbuf)*8; // 128*8
        //t.tx_buffer=sendbuf;
        //Wait for slave to be ready for next byte before sending

        //spi_device_transmit(handle, &t);
    }

    //Never reached.
    spi_bus_remove_device(handle);
}
